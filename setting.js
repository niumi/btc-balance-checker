// Google Analytics
var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-137927758-1']);
_gaq.push(['_trackPageview']);

(function () {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = 'https://ssl.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();


$(document).ready(function () {

    var addressRegex = /^[13][a-km-zA-HJ-NP-Z1-9]{25,34}$/;

    var premiumXpubAlert = '<div class="alert alert-warning alert-dismissible fade show" role="alert"><small>Free version does not support checking balance by xpub. Send <a href="mailto:niumi.workshop@gmail.com">niumi.workshop@gmail.com</a> a email to request a premium version, $1.99/mon.</small><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
    var successAlert = '<div class="alert alert-success alert-dismissible fade show" role="alert"> Addresses added success, check your new balance. <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button> </div>';
    var port = chrome.extension.connect({
        name: "Sample Communication"
    });

    var background = chrome.extension.getBackgroundPage();
    var tipEle = $('.token-tip');

    function initToken() {
        if (background.tokenRecord) {
            $('#token').val(background.tokenRecord.token);
            showTip("Your token will expired at: " + new Date(background.tokenRecord.expiredAt).toLocaleDateString());
        }
    }

    initToken();

    function showTip(text, type) {

        if (type === "error") {
            tipEle.addClass('red');
        } else {
            tipEle.removeClass('red');
        }
        tipEle.text(text);
        tipEle.removeClass('d-none');
    }

    function hideTip() {
        tipEle.addClass("d-none");
    }

    $('.save').click(function (event) {
        event.stopPropagation();
        hideTip();
        var token = $('#token').val();
        if (!token) {
            showTip('Token is invalid', "error");
            return false;
        }
        background.requestToken(token, function (rsp, error) {
            if (!rsp) {
                showTip('Some error happen', "error");
                return;
            } else if (background.isExpired(new Date(rsp.expiredAt))) {
                showTip("This token is expired", "error");
                return;
            } else {
                showTip("Your token will expired at: " + new Date(rsp.expiredAt).toLocaleDateString());
                port.postMessage({ key: "set_token", value: rsp });
            }
        });
        return false;
    });

    $('.clean-token').click(function (event) {
        event.stopPropagation();
        hideTip();
        port.postMessage({ key: "set_token", value: null });
        return false;
    });

});

