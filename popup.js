// Google Analytics
var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-137927758-1']);
_gaq.push(['_trackPageview']);

(function () {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = 'https://ssl.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();


$(document).ready(function () {

    new ClipboardJS('.copy-btn');
    var addressRegex = /^[13][a-km-zA-HJ-NP-Z1-9]{25,34}$/;

    var premiumWalletCountAlert = '<div class="alert alert-warning alert-dismissible fade show" role="alert"><small>Free version only allow adding two wallets. Send <a href="mailto:niumi.workshop@gmail.com">niumi.workshop@gmail.com</a> a email to request a premium version, $1.99/mon.</small><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
    var premiumXpubAlert = '<div class="alert alert-warning alert-dismissible fade show" role="alert"><small>Free version does not support checking balance by xpub. Send <a href="mailto:niumi.workshop@gmail.com">niumi.workshop@gmail.com</a> a email to request a premium version, $1.99/mon.</small><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
    var expiredAlert = '<div class="alert alert-warning alert-dismissible fade show" role="alert"> Your token is expired, some addresses will not sync. click <a href="https://niumiws.com/ccbc/payments">here</a> to upgrade a unlimited version. <button type="button" class="close close-expired-button" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button> </div>';
    var port = chrome.extension.connect({
        name: "Sample Communication"
    });

    var background = chrome.extension.getBackgroundPage();
    
    background.checkBalance();
    var availableWalletIds = background.getAvailableWalletIds();

    function isExpired() {
        var isExpired = !background.isValidToken();
        console.log("not show expired alert: " + background.notShowExpiredAlert);
        if (isExpired && !background.notShowExpiredAlert) {
            $('.free-trial-alert').html(expiredAlert);
        }
    }

    function showSum() {
        var exchangeRate = "BTC Current Price: <strong>" + background.currentPrice + "</strong>";
        var colorClass = background.isUp ? "green" : "red";
        var arrow = background.isUp ? "↑" : "↓";
        exchangeRate += ' <strong class="' + colorClass + '">'  + background.rateChange + arrow + "</strong>";
        var balance = 'Total Balance: <strong>' + background.lastUsdBalance + "</strong>";
        $('.sum').html(exchangeRate + "    " + balance);
        $('.timestamp').text("Update time: " + background.timestamp.toLocaleString());
    }

    function refillTable() {
        $(".wallet-list").html('');
        var wallets = background.wallets;
        var walletIds = background.walletIds;
        
        console.log(availableWalletIds);
        for (var i = 0; i < walletIds.length; i++) {
            var id = walletIds[i];
            var wallet = wallets[id];
            var operator = '<td style="width:18%; text-align: center"><a href="javascript:void(0)" class="copy-btn" data-clipboard-target="#addr-' + id + '">Copy</a> <a href="javascript:void(0)" class="del" data-wallet-id=' + id + '>Del</a></td>';
            var balance = availableWalletIds.indexOf(id) != -1 ? wallet.usdBalance : "--"
            $(".wallet-list").append("<tr><td style='width:15%'>" + wallet.name + "</td><td class='address-td' id='addr-" + id + "'>" + wallet.address + "</td><td style='width:15%'>" + balance + "</td>" + operator + "</tr>");
        }    
    }

    isExpired();
    showSum();
    // refillTable();

    chrome.runtime.onMessage.addListener(function (message, sender, sendResponse) {
        showSum();
        refillTable();
        $('.refresh').prop('disabled', false);
        $(".spinner").addClass("d-none");
    });

    $('.add').click(function () {
        // if (background.wallets.length >= 2) {
        //     $('.premium-alert').html(premiumWalletCountAlert);
        //     return false;
        // }
        // $('[class^="err"]').addClass('d-none');
        // $('.wallet-list-wrapper').addClass('d-none');
        // $('.edit-wrapper').removeClass('d-none');
        chrome.tabs.create({url: "./address-editor.html"});
    });

    $('.save').click(function (event) {
        event.stopPropagation();
        $('[class^="err"]').addClass('d-none');
        
        var name = $('#walletName').val();
        var address = $('#walletAddress').val();
        // if (address.indexOf('xpub')== 0) {
        //     $('.premium-alert').html(premiumXpubAlert);
        //     return false;
        // }

        if (!name) {
            $('.err-wallet-name').removeClass('d-none');
            return false;
        }
        if (!addressRegex.test(address) && address.indexOf('xpub') != 0) {
            $('.err-wallet-address').removeClass('d-none');
            return false;
        }
        var wallet = {
            name,
            address,
            chain:"btc"
        };
        $('.wallet-list-wrapper').removeClass('d-none');
        $('.edit-wrapper').addClass('d-none');
        // var operator = '<td><button type="button" class="btn btn-link btn-sm">Del</button></td>';
        // $(".wallet-list tr:last").after("<tr><td>" + wallet.name + "</td><td>" + wallet.address + "</td><td>" + wallet.balance + "</td>" + operator + "</tr>");
        port.postMessage({ key: "add_wallet", value: wallet });
        $('#walletName').val('');
        $('#walletAddress').val('');
        return false;
    });

    $('.cancel').click(function (event) {
        $('.wallet-list-wrapper').removeClass('d-none');
        $('.edit-wrapper').addClass('d-none');
        return false;
    });

    $('.refresh').click(function () {
        $(this).prop('disabled', true);
        $(".spinner").removeClass("d-none");
        port.postMessage({ key: "action", value: "refresh" });
    });

    $('.wallet-list').on('click', '.del', function(event) {
        port.postMessage({key: "delete_wallet", value: $(this).data("wallet-id")});
        return false;
    });

    $('.setting').click(function() {
        chrome.tabs.create({url: "./setting.html"});
    });

    $('body').on('click', 'a', function(){
        var href = $(this).attr('href');
        if (href == "javascript:void(0)" || href == "#") {
            return false;
        }
        
        chrome.tabs.create({url: $(this).attr('href')});
        return false;
    });

    $('body').on('click', '.close-expired-button', function(){
        port.postMessage({key: "setting_not_show_expired_alert", value: true});
    });

});

