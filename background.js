// background.js
var wallets = {};
var walletIds = [];
var totalBalance = 0;
var lastUsdBalance = "--";
var rateChange = "--";
var currentPrice = "--";
var isUp = true;
var timestamp = new Date("2019-04-06");
var lastBtcPriceBN, currentPriceBN;
var addressChecked = false;
var xpubChecked = false;
var tokenRecord;
var lastCheckTokenTime;
var notShowExpiredAlert;

chrome.runtime.onMessage.addListener(
    function (request, sender, sendResponse) {
        if (request.message === "clicked_browser_action") {
            var firstHref = $("a[href^='http']").eq(0).attr("href");
            chrome.runtime.sendMessage({ "message": "open_new_tab", "url": firstHref });
        }
    }
);

chrome.storage.sync.get(["ccbc_wallets", "ccbc_wallet_ids", "ccbc_last_refresh_time", "ccbc_token", "ccbc_last_check_token_time", "ccbc_not_show_expired_alert"], function (result) {
    wallets = result.ccbc_wallets || {};
    walletIds = result.ccbc_wallet_ids || [];
    var lastRefreshTime = result.ccbc_last_refresh_time;
    if (lastRefreshTime) {
        timestamp = new Date(lastRefreshTime);
    } else {
        timestamp = new Date("2019-04-06");
    }
    lastCheckTokenTime = result.ccbc_last_check_token_time;
    if (lastCheckTokenTime) {
        lastCheckTokenTime = new Date(lastCheckTokenTime);
    } else {
        lastCheckTokenTime = new Date("2019-04-06");
    }

    if (result.ccbc_token) {
        tokenRecord = result.ccbc_token;
    } else {
        tokenRecord = {
                token:"FREETOKEN",
                expiredAt: "2019-05-15"
        };
    }
    
    notShowExpiredAlert = !!result.ccbc_not_show_expired_alert;

});

function getAvailableWalletIds() {
    if (isValidToken()) {
        return walletIds;
    } else {
        var availableWalletIds = [];
        var i = 0;
        for (var id in walletIds) {
            if (i >= 2) break;
            var walletId = walletIds[id];
            if (walletId.split('_')[1].indexOf('xpub') == 0) continue;
            availableWalletIds.push(walletId);
            i++;
        }
        return availableWalletIds;
    }
}

function minutesDiff(date1, date2) {
    var difference = date1.getTime() - date2.getTime(); // This will give difference in milliseconds
    return Math.round(difference / 60000);
}

function concatAddress(isXpub) {
    if (getAvailableWalletIds().length == 0) "";
    var addresses = "";
    var xpubIdx = isXpub ? 0 : -1;
    var i = 0;
    var availableWalletIds = getAvailableWalletIds();
    for (var id in availableWalletIds) {
        var walletId = availableWalletIds[id];
        var wallet = wallets[walletId];
        if (wallet.address.indexOf('xpub') !== xpubIdx) continue;
        if (i != 0) {
            addresses += "|";
        }
        addresses += wallet.address;
        i++;
    }
    return addresses;
}

function checkPrice(callback) {
    var rateReq = "https://api.coindesk.com/v1/bpi/currentprice/usd.json";
    $.get(rateReq, function (response) {
        var rate = JSON.parse(response).bpi.USD.rate_float;
        currentPriceBN = new BigNumber(rate);
        currentPrice = numeral(currentPriceBN.toFormat(4)).format('$0,0.00');

        callback();
    });
}

function alertIfBalanceChanged() {
    var changedWallets = [];
    for (var id in getAvailableWalletIds()) {
        var walletId = getAvailableWalletIds()[id];
        var wallet = wallets[walletId];
        if (wallet.oldBalance != null) {
            var diff = wallet.balance.minus(wallet.oldBalance);
            if (!diff.isZero()) {
                var name = wallet.name;
                var changeFlag = diff.isPositive() ? "Received" : "Sended";
                var value = diff.abs().toFormat(4);
                changedWallets.push({
                    title: name,
                    message: changeFlag + " " + value + " BTC"
                });
            }
        }

    }
    if (changedWallets.length) {
        var opt = {
            type: "list",
            title: "BTC Balance Changed",
            message: "Your wallet balance changed",
            iconUrl: 'icon.png',
            items: changedWallets
        };
        chrome.notifications.create(opt);
    }
}

function finishChecking() {
    if (!addressChecked || !xpubChecked) {
        return;
    }

    for (var id in getAvailableWalletIds()) {
        var walletId = getAvailableWalletIds()[id];
        wallets[walletId]["usdBalance"] = numeral(wallets[walletId]["balance"].multipliedBy(currentPriceBN).toFormat(2)).format('$0,0.00');
    }
    var usdBalance = totalBalance.multipliedBy(currentPriceBN);
    var usdNumberal = numeral(usdBalance.toFormat(2));
    lastUsdBalance = usdNumberal.format('$0,0.00');


    var resultInMinutes = minutesDiff(new Date(), timestamp)
    if (resultInMinutes >= 30) {
        var content = "BTC Current Price: " + currentPrice + "\n";
        content += "Total Balance: " + lastUsdBalance;
        var opt = {
            type: "basic",
            title: "BTC balance changed",
            message: content,
            iconUrl: 'icon.png'
        };
        chrome.notifications.create(opt);
        lastBtcPriceBN = currentPriceBN;
    } else if (lastBtcPriceBN && !lastBtcPriceBN.isZero() && lastBtcPriceBN.minus(currentPriceBN).abs().gte(new BigNumber(50))) {
        var content = "BTC Current Price: " + currentPrice + "\n";
        content += "Total Balance: " + lastUsdBalance;
        var opt = {
            type: "basic",
            title: "Price changed more than $50",
            message: content,
            iconUrl: 'icon.png'
        };
        chrome.notifications.create(opt);
        lastBtcPriceBN = currentPriceBN;
    }
    alertIfBalanceChanged();
    timestamp = new Date();
    chrome.storage.sync.set({ "ccbc_last_refresh_time": timestamp.valueOf() })
    chrome.runtime.sendMessage({ type: 'refresh_complete' });
}

function checkAddressBalance(isXpub) {
    var addresses = concatAddress(isXpub);
    if (addresses) {
        var btcBalanceReq = "https://blockchain.info/balance?active=" + addresses;
        addressBalance = new BigNumber(0);
        $.get(btcBalanceReq, function (balances) {
            for (var address in balances) {
                var walletId = "btc" + "_" + address;
                wallets[walletId]["oldBalance"] = wallets[walletId]["balance"];
                var balance = balances[address];
                var finalBalance = new BigNumber(balance.final_balance).dividedBy(new BigNumber('1e8'));
                wallets[walletId]["balance"] = finalBalance;
                addressBalance = addressBalance.plus(finalBalance);
            }
            totalBalance = addressBalance.plus(totalBalance);
            if (isXpub) {
                xpubChecked = true;
            } else {
                addressChecked = true;
            }
            finishChecking();
        });
    } else {
        if (isXpub) {
            xpubChecked = true;
        } else {
            addressChecked = true;
        }
        finishChecking();
    }
}

function checkYesterdayPrice() {
    var yesterdayRateReq = "https://api.coindesk.com/v1/bpi/historical/close.json\?index\=USD\&for\=yesterday";
    $.get(yesterdayRateReq, function (response) {
        var bip = JSON.parse(response).bpi;
        var yesterdayPrice;
        for (var prop in bip) {
            yesterdayPrice = new BigNumber(bip[prop]);
            break;
        }
        var rateChangeBN = currentPriceBN.dividedBy(yesterdayPrice).minus(new BigNumber(1));
        isUp = rateChangeBN.isPositive();
        rateChange = rateChangeBN.toFormat(4);
        chrome.runtime.sendMessage({ type: 'refresh_complete' });

    });
}

function isExpired(expiredAt) {
    var result = minutesDiff(expiredAt, new Date());
    return result <= 0;
}

function requestToken(token, callback) {
    $.get("https://niumiws.com/ccbc/payments/" + token, callback);
}

function isValidToken() {
    if (tokenRecord) {
        if (isExpired(new Date(tokenRecord.expiredAt))) {
            return false;
        } else {
            return true;
        }

    } else {
        return false;
    }
}

function checkToken() {
    if (!tokenRecord) return;
    var diff = minutesDiff(new Date(), lastCheckTokenTime);
    if (diff >= 60 * 4) {
        requestToken(tokenRecord.token, function (rsp) {
            tokenRecord = rsp;
            lastCheckTokenTime = new Date();
            save({ "ccbc_token": rsp, "ccbc_last_check_token_time":  lastCheckTokenTime.valueOf() });
        });
    }
}

function ticker() {
    checkToken();
    checkBalance();
}

function checkBalance() {
    if (walletIds.length == 0) return;
    if (!lastBtcPriceBN) {
        lastBtcPriceBN = new BigNumber(0);
    }
    totalBalance = new BigNumber(0);
    checkPrice(function () {
        addressChecked = false;
        checkAddressBalance(false);
        xpubChecked = false;
        checkAddressBalance(true);
        checkYesterdayPrice();
    });

}

function saveWallets() {
    save({ "ccbc_wallets": wallets, "ccbc_wallet_ids": walletIds });
}

function save(obj) {
    chrome.storage.sync.set(obj);
}

chrome.extension.onConnect.addListener(function (port) {
    port.onMessage.addListener(function (msg) {
        if (msg.key === "add_wallet") {
            var wallet = msg.value;
            var walletId = wallet.chain + "_" + wallet.address;
            wallets[walletId] = wallet;
            if (walletIds.indexOf(walletId) == -1) {
                walletIds.push(walletId);
                saveWallets();
            }
        }
        if (msg.key === "add_wallets") {
            var newWallets = msg.value;
            for (var i = 0; i < newWallets.length; i++) {
                var wallet = newWallets[i];
                var walletId = wallet.chain + "_" + wallet.address;
                if (walletIds.indexOf(walletId) != -1) {
                    continue;
                }
                wallets[walletId] = wallet;
                walletIds.push(walletId);
            }
            saveWallets();
        }
        if (msg.key === "delete_wallet") {
            var i = 0;

            for (; i < walletIds.length; i++) {
                if (walletIds[i] == msg.value) {
                    break;
                }
            }
            walletIds.splice(i, 1);
            delete wallets[msg.value];
            saveWallets();
        }

        if (msg.key === "set_token") {
            lastCheckTokenTime = new Date();
            tokenRecord = msg.value;
            notShowExpiredAlert = false;
            save({ "ccbc_token": msg.value, "ccbc_last_check_token_time":  lastCheckTokenTime.valueOf(), "ccbc_not_show_expired_alert": false });
            return;
        }

        if (msg.key === "setting_not_show_expired_alert") {
            notShowExpiredAlert = true;
            save({"ccbc_not_show_expired_alert": true});
            return;
        }

        checkBalance();
        chrome.runtime.sendMessage({ type: 'refresh_complete' });

    });
})


chrome.alarms.onAlarm.addListener(ticker);

chrome.alarms.create("check", { periodInMinutes: 1 });
