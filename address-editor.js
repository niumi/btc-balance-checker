// Google Analytics
var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-137927758-1']);
_gaq.push(['_trackPageview']);

(function () {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = 'https://ssl.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();


$(document).ready(function () {

    var addressRegex = /^[13][a-km-zA-HJ-NP-Z1-9]{25,34}$/;

    var premiumXpubAlert = '<div class="alert alert-warning alert-dismissible fade show" role="alert"><small>Free version does not support checking balance by xpub. Send <a href="mailto:niumi.workshop@gmail.com">niumi.workshop@gmail.com</a> a email to request a premium version, $1.99/mon.</small><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
    var successAlert = '<div class="alert alert-success alert-dismissible fade show" role="alert"> Addresses added success, check your new balance. <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button> </div>';
    var port = chrome.extension.connect({
        name: "Sample Communication"
    });

    var background = chrome.extension.getBackgroundPage();

    $('.save').click(function (event) {
        event.stopPropagation();
        $('[class^="err"]').addClass('d-none');
        var wallets = [];
        for (var i=1; i<=10; i++) {
            var name = $.trim($('#wallet-name-' + i).val());
            var address = $.trim($('#wallet-address-' + i).val());
            if (!name && !address) {
                break;
            }
        
            if (!name) {
                $('.err-wallet-name-' +i).removeClass('d-none');
                return false;
            }
            if (!addressRegex.test(address) && address.indexOf('xpub') != 0) {
                $('.err-wallet-address-' + i).removeClass('d-none');
                return false;
            }

            if (address.indexOf("bc") == 0) {
                $('.err-wallet-address-' + i).removeClass('d-none');
                return false;
            }

            var wallet = {
                name,
                address,
                chain:"btc"
            };
            wallets.push(wallet);
            $('.success-alert').html(successAlert);
        }
        
        // var operator = '<td><button type="button" class="btn btn-link btn-sm">Del</button></td>';
        // $(".wallet-list tr:last").after("<tr><td>" + wallet.name + "</td><td>" + wallet.address + "</td><td>" + wallet.balance + "</td>" + operator + "</tr>");
        port.postMessage({ key: "add_wallets", value: wallets });
        return false;
    });

    $('.cancel').click(function (event) {
        $('.wallet-list-wrapper').removeClass('d-none');
        $('.edit-wrapper').addClass('d-none');
        return false;
    });

  
    $('.add-multi').click(function () {
        chrome.tabs.create({url: "./address-editor.html"});
    });

    $('body').on('click', 'a', function(){
        chrome.tabs.create({url: $(this).attr('href')});
        return false;
    });

});

